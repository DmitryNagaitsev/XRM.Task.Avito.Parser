﻿using System.Data.Entity;

namespace AdvertisementEntity
{
    public class AdvertisementContext : DbContext
    {

        public AdvertisementContext()
            : base("AdvertisementContext")
        { }

        public DbSet<AdvertisementEntity> Advertisements { get; set; }

    }
}
