﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisementEntity
{
    public class AdvertisementEntity
    {
        public int Id { get; set; }

        public int Code { get; set; }

        [Index]
        [MaxLength(250)]
        public string Title { get; set; }

        public string First_name { get; set; }

        public string Middle_name { get; set; }

        public string Last_name { get; set; }

        public string Reward { get; set; }

        public string Description { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Site { get; set; }

    }
}
