﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TaskXRM.Models.Advertisement;

namespace TaskXRM.Controllers.AdvertisementControllers
{
    public class AdvertisementEntitiesController : Controller
    {
        private AdvertisementContext db = new AdvertisementContext();

        // GET: AdvertisementEntities
        public ActionResult Index()
        {
            return View(db.Advertisements.ToList());
        }

        // GET: AdvertisementEntities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementEntity advertisementEntity = db.Advertisements.Find(id);
            if (advertisementEntity == null)
            {
                return HttpNotFound();
            }
            return View(advertisementEntity);
        }

        // GET: AdvertisementEntities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AdvertisementEntities/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Code,Title,First_name,Middle_name,Last_name,Reward,Description,Mobile,Email,Site")] AdvertisementEntity advertisementEntity)
        {
            if (ModelState.IsValid)
            {
                db.Advertisements.Add(advertisementEntity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(advertisementEntity);
        }

        // GET: AdvertisementEntities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementEntity advertisementEntity = db.Advertisements.Find(id);
            if (advertisementEntity == null)
            {
                return HttpNotFound();
            }
            return View(advertisementEntity);
        }

        // POST: AdvertisementEntities/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Code,Title,First_name,Middle_name,Last_name,Reward,Description,Mobile,Email,Site")] AdvertisementEntity advertisementEntity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertisementEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(advertisementEntity);
        }

        // GET: AdvertisementEntities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementEntity advertisementEntity = db.Advertisements.Find(id);
            if (advertisementEntity == null)
            {
                return HttpNotFound();
            }
            return View(advertisementEntity);
        }

        // POST: AdvertisementEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AdvertisementEntity advertisementEntity = db.Advertisements.Find(id);
            db.Advertisements.Remove(advertisementEntity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
