﻿using System.Linq;
using System.Web.Mvc;
using Advertisement;
using AdvertisementEntity;

namespace AdvertisementASP.Controllers.Advertisements
{
    public class AdvertisementEntitiesController : Controller
    {
        private readonly AdvertisementRepository _repository;
        //private AdvertisementContext db = new AdvertisementContext();

        public AdvertisementEntitiesController()
        {
            _repository = new AdvertisementRepository(new AdvertisementContext());
        }

        public AdvertisementEntitiesController(AdvertisementRepository repository)
        {
            _repository = repository;
        }

        // GET: AdvertisementEntities
        public ViewResult Index(string searchString)
        {
            var advs = from a in _repository.GetAdvertisements()
                select a;
            if (!string.IsNullOrEmpty(searchString))
            {
                advs = advs.Where(s => s.Title.ToUpper().Contains(searchString.ToUpper()));
            }

            return View(advs);
        }

        // GET: AdvertisementEntities/Details/5
        public ActionResult Details(int id)
        {
            AdvertisementEntity.AdvertisementEntity advertisementEntity = _repository.GetAdvertisementById(id);
            if (advertisementEntity == null)
            {
                return HttpNotFound();
            }
            return View(advertisementEntity);
        }


        // GET: AdvertisementEntities/Delete/5
        public ActionResult Delete(int id)
        {
            var advertisementEntity = _repository.GetAdvertisementById(id);
            if (advertisementEntity == null)
            {
                return HttpNotFound();
            }
            return View(advertisementEntity);
        }

        // POST: AdvertisementEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _repository.DeleteAdvertisement(id);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}