﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TaskXRM.Startup))]
namespace TaskXRM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
