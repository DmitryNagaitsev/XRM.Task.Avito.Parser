﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TaskXRM.Models.Advertisement
{
    public class AdvertisementContext:DbContext
    {
        public AdvertisementContext()
                : base("AdvertisementContext")
        { }

        public DbSet<AdvertisementEntity> Advertisements { get; set; }
    }
}