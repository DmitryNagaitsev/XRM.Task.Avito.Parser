﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using Advertisement;
using AdvertisementEntity;
using HtmlAgilityPack;

namespace DBFilling
{
    internal class Parser
    {
        public static AdvertisementRepository Repository { get; } =
            new AdvertisementRepository(new AdvertisementContext());

        public static Queue<string> UrlsCollection { get; } = new Queue<string>();

        public static object Locker { get; } = new object();

        public static string SiteUrl { get; } = "https://www.avito.ru";

        public static int PagesCount { get; set; }

        public static int AdvertisememntCount { get; set; }

        public static int ThreadsFinished { get; set; }

        private static void Main()
        {
            Console.WriteLine("Введите количество страниц которые необходимо загрузить: ");
            try
            {
                PagesCount = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(0);
            }
            Console.WriteLine(
                "Начало работы парсера, это может занять некоторое время. Не завершайте работу до того как все потоки будут завершены");
            for (var i = 1; i <= PagesCount; ++i)
            {
                GetUrls(i);
            }
            Console.ReadKey();
        }

        /// <summary>
        ///     GetUrls - это асинхронный метод, который получает все ссылки объявлений с сайта avito.ru/ekaterinburg
        ///     и сохраняет их в очереди urls_collection.
        /// </summary>
        /// <param name="pageNumber">номер страницы на сайте</param>

        public static async void GetUrls(int pageNumber)
        {
            var httpClient = new HttpClient();
            //
            //full query: "https://www.avito.ru/ekaterinburg?p=%pageNumber"
            //
            var page = await httpClient.GetStringAsync(SiteUrl + "/ekaterinburg?p=" + pageNumber);
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(page);
            var refs = htmlDocument.DocumentNode.SelectNodes(
                "//a[@class='item-description-title-link']"
                );
            if (refs != null)
            {
                foreach (var reference in refs)
                {
                    UrlsCollection.Enqueue(reference.Attributes["href"].Value);
                    //Добавляем задание на обработку очередной ссылки в пул потоков.
                    ThreadPool.QueueUserWorkItem(ParseItem);
                }
                Console.WriteLine($"{refs.Count} ссылок было добавлено");
                AdvertisememntCount += refs.Count;
            }
        }

        /// <summary>
        ///     ParseItem - это асинхронный метод, который достает из очереди одну ссылку, получает по ней страницу
        ///     и обрабатывает данные по определенному шаблону.
        /// </summary>
        /// <param name="arg">нужен для использования в пуле потоков</param>

        public static async void ParseItem(object arg)
        {
            string url;
            var page = "";
            string str;
            var adv = new AdvertisementEntity.AdvertisementEntity();
            var htmlDocument = new HtmlDocument();

            lock (Locker)
            {
                url = UrlsCollection.Dequeue();
            }

            var httpClient = new HttpClient();
            try
            {
                page = await httpClient.GetStringAsync(SiteUrl + url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            htmlDocument.LoadHtml(page);


            //Уникальный (int) идентификатор объявления (Model: Code)

            adv.Code = int.Parse(url.Split('_').Last());


            //Получаем заголовок объявления (Model: Title)

            var node = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"item\"]/div[4]/div[1]/div[2]/h1");
            adv.Title = node?.InnerText?.Trim();


            //Получаем имя продавца или название компании (Model: First_name)
            //Avito не сообщает полной информации о пользователе (фамилия, отчество, телефон)
            //для не зарегистрировавшихся

            node = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"seller\"]/*[1]");
            if (node != null)
            {
                str = node.InnerText.Trim();
                adv.First_name = str;
            }


            //Получаем цену товара или желаемую зарплату в резюме (Model: Reward)

            node =
                htmlDocument.DocumentNode.SelectSingleNode(
                    "//*[@id=\"item\"]/div[4]/div[1]/div[2]/div[2]/div[3]/div[1]/div[1]/div[2]/span/span");
            adv.Reward = node?.InnerText?.Trim();


            //Получаем текст объявления (Model: Description)

            node = htmlDocument.DocumentNode.SelectSingleNode("//*[contains(@class, 'g_b_16')]");

            if (node != null)
            {
                str = node.InnerText;
                str = str.Trim();
                str = Regex.Replace(str, "\n\\s+", "\n");
                adv.Description = str;
            }


            //Сохраняем ссылку на объявление (Model: Site)
            adv.Site = SiteUrl + url;

            lock (Locker)
            {
                Repository.Store(adv);
                ThreadsFinished++;
                if (ThreadsFinished == AdvertisememntCount)
                {
                    Repository.Dispose();
                }
            }
        }
    }
}