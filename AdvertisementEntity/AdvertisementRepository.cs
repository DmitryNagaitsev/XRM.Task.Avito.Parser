﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdvertisementEntity;

// ReSharper disable once CheckNamespace
namespace Advertisement
{
    public class AdvertisementRepository : IDisposable
    {
        private readonly AdvertisementContext _context;

        private List<AdvertisementEntity.AdvertisementEntity> advertisements = new List<AdvertisementEntity.AdvertisementEntity>();

        public AdvertisementRepository(AdvertisementContext context)
        {
            _context = context;
        }

        public IEnumerable<AdvertisementEntity.AdvertisementEntity> GetAdvertisements()
        {
            return _context.Advertisements.ToList();
        }

        public AdvertisementEntity.AdvertisementEntity GetAdvertisementById(int id)
        {
            return _context.Advertisements.Find(id);
        }

        public void Store(AdvertisementEntity.AdvertisementEntity adv)
        {
            if (!_context.Advertisements.Any(x => x.Code == adv.Code))
            {
                advertisements.Add(adv);
                if (advertisements.Count >= 100)
                {
                    InsertAdvertisements();
                    Save();
                    Console.WriteLine("added " + advertisements.Count + " entities");
                    advertisements.Clear();
                }
            }

        }

        public void InsertAdvertisement(AdvertisementEntity.AdvertisementEntity adv)
        {
            if (!_context.Advertisements.Any(x => x.Code == adv.Code))
                _context.Advertisements.Add(adv);
        }

        public void InsertAdvertisements()
        {
            _context.Advertisements.AddRange(advertisements);

        }

        public void DeleteAdvertisement(int advId)
        {
            AdvertisementEntity.AdvertisementEntity adv = _context.Advertisements.Find(advId);
            _context.Advertisements.Remove(adv);
        }

        /// <summary>
        /// Not implemented
        /// </summary>

        //public void UpdateAdvertisement(AdvertisementEntity adv)
        //{
        //    _context.Entry(adv).State = System.Data.Entity.EntityState.Modified;
        //}

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            InsertAdvertisements();
            Save();
            Console.WriteLine("added " + advertisements.Count + " entities");
            Dispose(true);
            GC.SuppressFinalize(this);
            Console.WriteLine("Все потоки были завершены");
        }
    }
}
